import React, { useState, useRef, useEffect } from 'react'
import styled from '@emotion/styled'
import _ from 'lodash'

const LayoutWrapper = styled.div`
	background: #3f3f3f;
	margin: 0;
	padding: 1em;
	font-size: 16px;
	box-sizing: border-box;
	font-family: Roboto;
	min-height: 100vh;
	color: white;

	* {
		font-family: inherit;
	}
`

const Header = styled.h1`
	color: white;
	font-size: 40px;
`

const PaneLayout = styled.div`
	min-height: 200px;
	padding: 16px;
	background: #8f8f8f;
`

const PaneTitle = styled.div`
	margin-bottom: 10px;
`

const Input = styled.input`
	outline: none;
	height: 30px;
	width: 300px;
	padding: 8px;
	border: 1px solid #aaa;
	border-radius: 5px;
	box-sizing: border-box;
`

const HelperWrapper = styled.div`
	color: black;
	transition: all 0.4s;
	width: 200px;
	box-shadow: 1px 1px 10px 0 #555;
	margin-top: 2px;
	margin-left: ${props => props.marginLeft || '0'}px;

	background: #f5f5f5;
	min-height: 0;

	font-size: 12px;
	border-radius: 2px;
	overflow: hidden;
`

const HelperItem = styled.div`
	padding: 8px;
	background: #efefef;
`

const InputBoxWrapper = styled.div`
	position: relative;
	max-width: 300px;
`

const SuggestionWrapper = styled.div`
	color: black;
	transition: all 0.4s;
	width: 100%;
	margin-top: 2px;
	margin-left: ${props => props.marginLeft || '0'}px;
	border: 0;
	border-radius: 5px;
	box-sizing: border-box;
	box-shadow: 0px 5px 10px -3px #555;
	padding-top: 8px;
	padding-bottom: 8px;
	background: #fff;
	position: absolute;
	min-height: 0;

	font-size: 12px;
	overflow: hidden;
`

const SuggestionItem = styled.div`
	padding: 12px;
	background: ${props => (props.selected ? '#efefef' : '#fff')};
	cursor: pointer;
	font-weight: 500;

	&:hover {
		background: #efefef;
	}
`

const formulaList = _.orderBy(
	[
		{
			name: 'Power',
			parameters: ['number', 'power'],
		},
		{
			name: 'Log',
			parameters: ['number', 'base'],
		},
		{
			name: 'Sqrt',
			parameters: ['number'],
		},
		{
			name: 'Lag',
			parameters: ['number', 'period', 'timeUnit'],
		},
	],
	'name',
	'asc',
)

const App = () => {
	const formulaInputRef = useRef()
	const [formulaType, setFormulaType] = useState('')
	const [cursorPosition, setCursorPosition] = useState(0)
	const [currentParamIndex, setCurrentParamIndex] = useState(-1)
	const [shouldHighlightParam, setShouldHighlightParam] = useState(false)
	const [isFocused, setIsFocused] = useState(false)
	const [selectedSuggestionIndex, setSelectedSuggestionIndex] = useState(0)
	const [suggestionList, setSuggestionList] = useState([])

	function handleChange(e) {
		const formula = formulaInputRef.current || {}
		if (e.key === 'ArrowDown') {
			e.preventDefault()
			setSelectedSuggestionIndex(
				selectedSuggestionIndex + 1 <= suggestionList.length ? selectedSuggestionIndex + 1 : 0,
			)
		}

		if (e.key === 'ArrowUp') {
			e.preventDefault()
			setSelectedSuggestionIndex(
				selectedSuggestionIndex - 1 >= 0 ? selectedSuggestionIndex - 1 : suggestionList.length,
			)
		}

		if (e.key === 'Enter') {
			const withSuggestion = selectedSuggestionIndex - 1 >= 0
			formula.value = withSuggestion
				? `${suggestionList[selectedSuggestionIndex - 1]}(`
				: formula.value
			setSelectedSuggestionIndex(0)
		}

		// SetTimeout 0 to await event happened
		setTimeout(() => {
			const formulaValue =
				(formula.value && formula.value.substring(0, formula.selectionStart)) || ''
			const reverseFormula = formulaValue.split('(').reverse()
			const [param] = reverseFormula

			const getParameterIndex = () => {
				const getParameter = () => {
					let closeParenthesisCount = 0
					// eslint-disable-next-line no-restricted-syntax
					for (const iterator of param) {
						if (iterator === ')') closeParenthesisCount += 1
					}

					return reverseFormula[closeParenthesisCount] || ''
				}

				const calculateIndex = parameter => {
					let index = 0
					let isFoundParenthesis = false

					const parameterReverse = parameter
						.split('')
						.reverse()
						.join('')

					// eslint-disable-next-line no-restricted-syntax
					for (const iterator of parameterReverse) {
						if (iterator === ')') {
							isFoundParenthesis = true
						}

						if (iterator === ',' && !isFoundParenthesis) {
							index += 1
						}
					}

					return index
				}

				const paramItem = getParameter()

				return paramItem !== param
					? calculateIndex(paramItem) + calculateIndex(param)
					: calculateIndex(param)
			}

			const normalizeParenthesisRecursive = parameter => {
				// eslint-disable-next-line no-restricted-syntax
				for (const iterator of parameter) {
					if (iterator === ')' && reverseFormula.length > 1) {
						const [removeItem] = reverseFormula.splice(1, 1)
						normalizeParenthesisRecursive(removeItem)
					}
				}
			}

			const paramIndex = getParameterIndex()
			normalizeParenthesisRecursive(param)

			const [parameters, type = ''] = reverseFormula
			const [currentType] = type.split(',').reverse()

			const suggestList = formulaList
				.map(formulaItem => formulaItem.name)
				.filter(name => name.toLowerCase().startsWith(formulaInputRef.current.value.toLowerCase()))

			setIsFocused(document.activeElement === formula)
			setCursorPosition(formula.selectionStart)
			setCurrentParamIndex(paramIndex)
			setShouldHighlightParam(!!type)
			setSuggestionList(suggestList)

			if (!isFocused) {
				setSelectedSuggestionIndex(0)
			}

			if (currentType) {
				let newFormulaTypes = []
				formulaList.forEach(formulaItem => {
					if (formulaItem.name.toLowerCase().endsWith(currentType.trim().toLowerCase())) {
						newFormulaTypes = [...newFormulaTypes, formulaItem]
					}
				})
				setFormulaType(newFormulaTypes)
			} else if (!currentType && parameters) {
				let newFormulaTypes = []
				formulaList.forEach(formulaItem => {
					if (formulaItem.name.toLowerCase().indexOf(parameters.toLowerCase()) > -1) {
						newFormulaTypes = [...newFormulaTypes, formulaItem]
					}
				})

				setFormulaType(newFormulaTypes)
			} else setFormulaType('')
		})
	}

	function handleClickSuggestionIndex(index) {
		formulaInputRef.current.value = `${suggestionList[index]}(`
		formulaInputRef.current.selectionStart = suggestionList[index].length + 1
		formulaInputRef.current.focus()
		setCursorPosition(suggestionList[index].length + 1)
		setSelectedSuggestionIndex(0)
	}

	useEffect(() => {
		window.addEventListener('keydown', handleChange)
		window.addEventListener('click', handleChange)

		return () => {
			window.removeEventListener('keydown', handleChange)
			window.removeEventListener('click', handleChange)
		}
	})

	return (
		<LayoutWrapper>
			<Header>Formula Draft</Header>
			<PaneLayout>
				<PaneTitle>Formula</PaneTitle>
				<InputBoxWrapper>
					<Input ref={formulaInputRef} />
					{isFocused && !shouldHighlightParam && suggestionList.length > 0 && (
						<SuggestionWrapper>
							{suggestionList.map((name, index) => {
								return (
									<SuggestionItem
										title={name}
										selected={selectedSuggestionIndex === index + 1}
										key={index}
										onClick={() => {
											handleClickSuggestionIndex(index)
										}}
									>
										{name}
									</SuggestionItem>
								)
							})}
						</SuggestionWrapper>
					)}
					{formulaType && shouldHighlightParam && isFocused && (
						<HelperWrapper marginLeft={cursorPosition * 2}>
							{formulaType.map(formulaItem => {
								return (
									<HelperItem key={formulaItem.name}>
										{formulaItem.name}(
										{formulaItem.parameters.map((parameter, index) => {
											const withCommaSpace = (index > 0 && ', ') || ''
											return shouldHighlightParam && currentParamIndex === index ? (
												<React.Fragment key={index}>
													{withCommaSpace}
													<strong>{parameter}</strong>
												</React.Fragment>
											) : (
												`${withCommaSpace}${parameter}`
											)
										})}
										)
									</HelperItem>
								)
							})}
						</HelperWrapper>
					)}
				</InputBoxWrapper>
			</PaneLayout>
		</LayoutWrapper>
	)
}

export default App
